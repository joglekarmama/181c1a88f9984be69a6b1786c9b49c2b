import React, { useEffect } from 'react';
import Timer from './components/Timer/timer';
import config from 'visual-config-exposer';

const App = () => {
  useEffect(() => {
    const bg = document.querySelector('main');
    bg.style.background = `url(${config.settings.bgImg})`;
    bg.style.backgroundRepeat = 'no-repeat';
    bg.style.backgroundSize = 'cover';
    bg.style.backgroundPosition = 'center';
  }, []);

  return (
    <main>
      <Timer />
    </main>
  );
};

export default App;
