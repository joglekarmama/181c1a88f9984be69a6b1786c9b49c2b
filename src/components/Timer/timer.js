import React, { useEffect } from 'react';
import config from 'visual-config-exposer';
import './timer.css';

const SECOND = 1000;
const MINUTE = SECOND * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;

const timer = () => {
  let timeLeft, endDate;
  let day, hour, minute, second;

  useEffect(() => {
    day = Array.from(document.querySelectorAll('.bloc-time.day .figure'));
    hour = Array.from(document.querySelectorAll('.bloc-time.hour .figure'));
    minute = Array.from(document.querySelectorAll('.bloc-time.min .figure'));
    second = Array.from(document.querySelectorAll('.bloc-time.sec .figure'));

    const captions = Array.from(document.querySelectorAll('.count-title'));
    captions.forEach(
      (caption) => (caption.style.color = config.settings.fontColor)
    );

    const inputDate = config.settings.date;
    const inputMonth = config.settings.month;
    const inputYear = config.settings.year;
    const inputHour = config.settings.hour;
    const inputMinute = config.settings.minute;
    const inputSecond = config.settings.second;

    const fullEndDate = `${inputDate} ${inputMonth}, ${inputYear} ${inputHour}:${inputMinute}:${inputSecond}`;
    endDate = new Date(fullEndDate);
  }, []);

  const animate = (element, value) => {
    const top = element.querySelector('.top');
    const topBack = element.querySelector('.top-back');
    const topSpan = element.querySelector('.top-back span');
    const bottom = element.querySelector('.bottom');
    // const bottomBack = element.querySelector('.bottom-back');
    const bottomSpan = element.querySelector('.bottom-back span');

    if (value == top.innerText) {
      return;
    }

    topSpan.innerText = `${value}`;
    bottomSpan.innerText = `${value}`;

    TweenMax.to(top, 0.8, {
      rotationX: '-180deg',
      transformPerspective: 300,
      ease: Quart.easeOut,
      onComplete: function () {
        top.innerText = value;
        bottom.innerText = value;
        TweenMax.set(top, { rotationX: 0 });
      },
    });

    TweenMax.to(topBack, 0.8, {
      rotationX: 0,
      transformPerspective: 300,
      ease: Quart.easeOut,
      clearProps: 'all',
    });
  };

  const checkTime = (element, time) => {
    let time1, time2, time3;
    if (time >= 100) {
      [time1, time2, time3] = time.toString();
      animate(element[0], time1);
      animate(element[1], time2);
      animate(element[2], time3);
    } else if (time >= 10) {
      [time1, time2] = time.toString();
      animate(element[0], time1);
      animate(element[1], time2);
    } else {
      time1 = time.toString();
      animate(element[0], '0');
      animate(element[1], time1);
    }
  };

  const changeTimeHandler = () => {
    timeLeft = endDate.getTime() - Date.now();

    checkTime(day, Math.floor(timeLeft / DAY));
    checkTime(hour, Math.floor((timeLeft % DAY) / HOUR));
    checkTime(minute, Math.floor((timeLeft % HOUR) / MINUTE));
    checkTime(second, Math.floor((timeLeft % MINUTE) / SECOND));
  };

  setInterval(changeTimeHandler, 1000);

  return (
    <div className="time-wrapper">
      <div
        className="main-title"
        style={{
          fontSize: config.settings.fontSize,
          color: config.settings.fontColor,
          fontFamily: config.settings.fontType,
        }}
      >
        {config.settings.title}
      </div>
      <time className="countdown">
        <div className="bloc-time day" data-init-value="24">
          <span className="count-title">Day</span>
          <div>
            <div className="figure day day-1">
              <span className="top">3</span>
              <span className="top-back">
                <span>3</span>
              </span>
              <span className="bottom">3</span>\
              <span className="bottom-back">
                <span>3</span>
              </span>
            </div>
            <div className="figure day day-2">
              <span className="top">1</span>
              <span className="top-back">
                <span>1</span>
              </span>
              <span className="bottom">1</span>
              <span className="bottom-back">
                <span>1</span>
              </span>
            </div>
            {Math.floor(timeLeft / DAY) >= 100 && (
              <div className="figure day day-3">
                <span className="top">1</span>
                <span className="top-back">
                  <span>1</span>
                </span>
                <span className="bottom">1</span>
                <span className="bottom-back">
                  <span>1</span>
                </span>
              </div>
            )}
          </div>
        </div>

        <div className="bloc-time hour" data-init-value="24">
          <span className="count-title">Hour</span>
          <div>
            <div className="figure hour hour-1">
              <span className="top">2</span>
              <span className="top-back">
                <span>2</span>
              </span>
              <span className="bottom">2</span>
              <span className="bottom-back">
                <span>2</span>
              </span>
            </div>
            <div className="figure hour hour-2">
              <span className="top">4</span>
              <span className="top-back">
                <span>4</span>
              </span>
              <span className="bottom">4</span>
              <span className="bottom-back">
                <span>4</span>
              </span>
            </div>
          </div>
        </div>
        <div className="bloc-time min" data-init-value="0">
          <span className="count-title">Minutes</span>
          <div>
            <div className="figure min min-1">
              <span className="top">0</span>
              <span className="top-back">
                <span>0</span>
              </span>
              <span className="bottom">0</span>
              <span className="bottom-back">
                <span>0</span>
              </span>
            </div>
            <div className="figure min min-2">
              <span className="top">0</span>
              <span className="top-back">
                <span>0</span>
              </span>
              <span className="bottom">0</span>
              <span className="bottom-back">
                <span>0</span>
              </span>
            </div>
          </div>
        </div>

        <div className="bloc-time sec" data-init-value="0">
          <span className="count-title">Seconds</span>
          <div>
            <div className="figure sec sec-1">
              <span className="top">0</span>
              <span className="top-back">
                <span>0</span>
              </span>
              <span className="bottom">0</span>
              <span className="bottom-back">
                <span>0</span>
              </span>
            </div>
            <div className="figure sec sec-2">
              <span className="top">0</span>
              <span className="top-back">
                <span>0</span>
              </span>
              <span className="bottom">0</span>
              <span className="bottom-back">
                <span>0</span>
              </span>
            </div>
          </div>
        </div>
      </time>
    </div>
  );
};

export default timer;
